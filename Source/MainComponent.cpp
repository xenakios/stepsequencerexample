/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"

//==============================================================================
MainComponent::MainComponent()
{
    setSize (800, 600);
	StringArray soundfiles{
		"C:\\MusicAudio\\Samples from net\\5206__rocktopus__kalimba-single-notes\\79794__rocktopus__untitled-18.aif",
		"C:\\MusicAudio\\Samples from net\\5206__rocktopus__kalimba-single-notes\\79815__rocktopus__untitled-38.aif" };
	AudioFormatManager afm;
	afm.registerBasicFormats();
	for (int i=0;i<soundfiles.size();++i)
	{
		auto reader = std::unique_ptr<AudioFormatReader>(afm.createReaderFor(File(soundfiles[i])));
		jassert(reader != nullptr);
		BigInteger range;
		range.setBit(60 + i);
		m_synth.addSound(new SamplerSound(soundfiles[i], *reader, range, 60 + i, 0.01, 0.01, 5.0));
	}
	for (int i = 0; i < 16; ++i)
	{
		m_synth.addVoice(new SamplerVoice);
	}
	// Using seconds here to time the events, could also use other units, as long as the getNextAudioBlock
	// calculates accordingly
	m_sequence.addEvent(MidiMessage::noteOn(1, 60, (uint8)127), 0.0);
	m_sequence.addEvent(MidiMessage::noteOff(1, 60, (uint8)0), 0.5);
	m_sequence.addEvent(MidiMessage::noteOn(1, 61, (uint8)127), 1.0);
	m_sequence.addEvent(MidiMessage::noteOff(1, 61, (uint8)0), 1.5);
	m_sequence.updateMatchedPairs();
	m_sequence.sort();
	setAudioChannels (2, 2);
}

MainComponent::~MainComponent()
{
    shutdownAudio();
}

//==============================================================================
void MainComponent::prepareToPlay (int samplesPerBlockExpected, double sampleRate)
{
	m_synth.setCurrentPlaybackSampleRate(sampleRate);
}

void MainComponent::getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill)
{
    bufferToFill.clearActiveBufferRegion();
	// This is somewhat inefficient and not entirely realtime safe, but serves for testing purposes
	MidiBuffer miditosynth;
	MidiMessageSequence tempsequence;
	double sr = deviceManager.getCurrentAudioDevice()->getCurrentSampleRate();
	tempsequence.addSequence(m_sequence, -m_midi_seq_playpos, 0.0, bufferToFill.numSamples / sr);
	for (auto& e : tempsequence)
		miditosynth.addEvent(e->message, 0);
	m_midi_seq_playpos += bufferToFill.numSamples / sr;
	if (m_midi_seq_playpos >= 2.0) // loop sequence at every 2 seconds, but it's not sample accurate like this...
	{
		m_midi_seq_playpos = 0.0;
	}
	m_synth.renderNextBlock(*bufferToFill.buffer, miditosynth, bufferToFill.startSample, bufferToFill.numSamples);
}

void MainComponent::releaseResources()
{
    
}

//==============================================================================
void MainComponent::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));

    // You can add your drawing code here!
}

void MainComponent::resized()
{
    // This is called when the MainContentComponent is resized.
    // If you add any child components, this is where you should
    // update their positions.
}
